# Utilisation
Lancez la commande ```dune build``` dans le repertoire dune_template
après Lancez la commande ```_build/default/bin/main.exe``` après copiez un lien gemini ('gemini://gemini.circumlunar.space/' par exemple) ou tapez exit pour quitter le programme

# Description
## update 13/04/2022
Le projet pour l'instant permet au client d'entrer le lien 
## update 20/04/2022
La commande 'exit' permet de quitter ou vous pouvez allez à un lien spécifique (gemini://gemini.circumlunar.space/ par exemple) et ajout des couleurs on a encore un problème : comment structurer le contenu (raw) de la réponse.

## update 25/04/2022
Le contenu est structuré avec un affichage à peu près pareil à Amfora.
